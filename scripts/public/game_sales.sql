create table public.game_sales
(
	"Id" text,
	game_name text,
	platform text,
	year text,
	genre text,
	publisher text,
	"NA+AF8-Sales" text,
	"EU+AF8-Sales" text,
	"JP+AF8-Sales" text,
	"Other+AF8-Sales" text,
	"Global+AF8-Sales" text
);

alter table public.game_sales owner to postgres;

