package net.devconf.demo.devconf_spark_demo;

import static org.apache.spark.sql.functions.expr;
import static org.apache.spark.sql.functions.concat;
import static org.apache.spark.sql.functions.element_at;
import static org.apache.spark.sql.functions.size;
import static org.apache.spark.sql.functions.lit;
import static org.apache.spark.sql.functions.split;
import static org.apache.spark.sql.functions.desc;

import java.util.Properties;

import org.apache.spark.Partition;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

/**
 * Union of two dataframes.
 * 
 * @author cwt
 */
public class GameDateApp {

  private SparkSession spark;

  /**
   * main() is your entry point to the application.
   * 
   * @param args
   */
  public static void main(String[] args) {
    GameDateApp app =
        new GameDateApp();
    app.start();
  }

  /**
   * The processing code.
   */
  private void start() {

    // Creates a session on a local master
    this.spark = SparkSession.builder()
        .appName("Game Data Processing")
        .master("local")
        .getOrCreate();

    Dataset<Row> csvDataFrame = buildCSVDataframe();
    Dataset<Row> dbDataFrame = buildDatabaseDataframe();

    combineDataframes(csvDataFrame, dbDataFrame);

  }

  /**
   * Performs the union between the two dataframes.
   * 
   * @param df1
   *          Dataframe to union on
   * @param df2
   *          Dataframe to union from
   */
  private void combineDataframes(Dataset<Row> df1, Dataset<Row> df2) {
    
    Dataset<Row> df = df2.unionByName(df1);
    
    df = df.withColumn("Total_Sales", expr("(NA_Sales+EU_sales+JP_Sales+Other_Sales)"))
    		.withColumnRenamed("NA_Sales", "North American Sales")
    		.withColumnRenamed("EU_sales", "European Sales")
    		.withColumnRenamed("JP_Sales", "Japanese Sales")
    		.withColumnRenamed("Other_Sales", "Other Sales")
    		.orderBy(desc("Total_Sales"));
               
    df.show(20);
    
    df.printSchema();
    System.out.println("We have " + df.count() + " records.");

    Partition[] partitions = df.rdd().partitions();
    int partitionCount = partitions.length;
    System.out.println("Partition count: " + partitionCount);
    
    //return df;
    
  }

  /**
   * Builds the dataframe containing csv data
   * 
   * @return A dataframe
   */
  private Dataset<Row> buildCSVDataframe() {
    
    Dataset<Row> df = this.spark.read()
        .format("csv")
        .option("header", "true")
        .load("data/games_1980-1994.csv");
    
    // Transform the dataframe
    df = df.withColumnRenamed("Name", "Game Name")
        .withColumnRenamed("Platform", "Platform")
        .withColumnRenamed("Year", "Release Year")
        .withColumnRenamed("Genre", "Genre")
        .withColumnRenamed("Publisher", "Publisher")
//        .withColumnRenamed("NA_Sales", "North American Sales")
//        .withColumnRenamed("EU_sales", "European Sales")
//        .withColumnRenamed("JP_Sales", "Japanese Sales")
//        .withColumnRenamed("Other_Sales", "Other Sales")
        
        //.drop("Global_Sales")
        .drop("Id")
        .drop("Rank");
    
    df.printSchema();
    
    // df = df.repartition(4);
    
    df.show(5);

    return df;
  }

  /**
   * Builds the dataframe containing data from a database
   * 
   * @return A dataframe
   */
  private Dataset<Row> buildDatabaseDataframe() {
       
    // The connection URL, assuming your PostgreSQL instance runs locally on
    // the
    // default port, and the database we use is "spark_labs"
    String dbConnectionUrl = "jdbc:postgresql://localhost:5432/spark_labs";

    // Properties to connect to the database, the JDBC driver is part of our
    // pom.xml
    Properties props = new Properties();
    props.setProperty("driver", "org.postgresql.Driver");
    props.setProperty("user", "postgres");
    props.setProperty("password", "demopassword");
        
    String sqlQuery = "(select * from public.game_sales) films";

    // Let's look for all movies matching the query
    Dataset<Row> df = spark.read().jdbc(
    	dbConnectionUrl,
        sqlQuery,
        props);
    
    df = df.withColumnRenamed("game_name", "Game Name")
            .withColumnRenamed("platform", "Platform")
            .withColumnRenamed("year", "Release Year")
            .withColumnRenamed("genre", "Genre")
            .withColumnRenamed("publisher", "Publisher")
            .withColumnRenamed("north_america_sales", "NA_Sales")
            .withColumnRenamed("european_sales", "EU_sales")
            .withColumnRenamed("japanese_sales", "JP_Sales")
            .withColumnRenamed("other_sales", "Other_Sales")
            .withColumn("Global_Sales", lit("0"))
            //.drop("global_sales")
            .drop("id");
    
    df.printSchema();
    
    // df = df.repartition(4);

    return df;
  }
}