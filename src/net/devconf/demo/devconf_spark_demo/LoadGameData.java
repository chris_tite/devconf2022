package net.devconf.demo.devconf_spark_demo;

import static org.apache.spark.sql.functions.lit;

import java.util.Properties;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

public class LoadGameData {

	private SparkSession spark;

	  /**
	   * main() is your entry point to the application.
	   * 
	   * @param args
	   */
	  public static void main(String[] args) {
		  LoadGameData app =
	        new LoadGameData();
	    app.start();
	  }

	  /**
	   * The processing code.
	   */
	  private void start() {

	    // Creates a session on a local master
	    this.spark = SparkSession.builder()
	        .appName("Game Data Loading")
	        .master("local")
	        .getOrCreate();

	    // Step 1: Ingestion
	    // ---------

	    // Reads a CSV file with header, called authors.csv, stores it in a
	    // dataframe
	    Dataset<Row> df = spark.read()
	        .format("csv")
	        .option("header", "true")
	        .load("data/games_1994+.csv");
	    
	    // Transform the dataframe
	    df = df.withColumnRenamed("HSISID", "datasetId")
	        .withColumnRenamed("Name", "game_name")
	        .withColumnRenamed("Platform", "platform")
	        .withColumnRenamed("Year", "year")
	        .withColumnRenamed("Genre", "genre")
	        .withColumnRenamed("Publisher", "publisher")
	        .withColumnRenamed("NA_Sales", "north_america_sales")
	        .withColumnRenamed("EU_sales", "european_sales")
	        .withColumnRenamed("JP_Sales", "japanese_sales")
	        .withColumnRenamed("Other_Sales", "other_sales")
	        .drop("Global_Sales")
	        .drop("Rank");
	    
	    // Step 2: Save
	    // ---------

	    // The connection URL, assuming your PostgreSQL instance runs locally on
	    // the
	    // default port, and the database we use is "spark_labs"
	    String dbConnectionUrl = "jdbc:postgresql://localhost/spark_labs";

	    // Properties to connect to the database, the JDBC driver is part of our
	    // pom.xml
	    Properties prop = new Properties();
	    prop.setProperty("driver", "org.postgresql.Driver");
	    prop.setProperty("user", "postgres");
	    prop.setProperty("password", "demopassword");

	    // Write in a table called ch02
	    df.write()
	        .mode(SaveMode.Overwrite)
	        .jdbc(dbConnectionUrl, "game_sales", prop);

	    System.out.println("Process complete");

	  }
	
}


